import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ClienteService } from '../service/cliente.service';
import { Cliente } from '../model/cliente.model';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.css']
})
export class CadastroClienteComponent implements OnInit {
  @ViewChild('FormCadastro', { static: true }) formCadastro: NgForm;

  cliente: Cliente;
  clienteExistente;

  constructor(
    public clienteService: ClienteService,
    public dialogRef: MatDialogRef<CadastroClienteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.cliente = new Cliente();

    if (this.data) {
      this.cliente = Object.assign(this.cliente, this.data.cliente);
    }
  }

  async verifyClienteByCpf(cpf: string) {
    this.clienteExistente = await this.clienteService.existsByCpf(cpf).toPromise();
  }

  async saveCliente() {
    if (this.cliente.id) {
      this.clienteService.put(this.cliente, this.cliente.id).subscribe(
        async (success) => {
          await Swal.fire({
            icon: 'success',
            title: 'Cliente alterado com sucesso',
            showConfirmButton: true,
          });

          this.closeDialog();
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            title: 'Não foi possível alterar o Cliente',
            showConfirmButton: true,
          });
        }
      );
    } else {
      await this.verifyClienteByCpf(this.cliente.cpf);
      if (this.clienteExistente === true) {
        Swal.fire({
          icon: 'warning',
          title: 'O CPF informado já está cadastrado',
          showConfirmButton: true,
        });
      } else {
        this.clienteService.post(this.cliente).subscribe(
           async (success) => {
            await Swal.fire({
              icon: 'success',
              title: 'Cliente cadastrado com sucesso',
              showConfirmButton: true,
            });

            this.closeDialog();
          },
          (error) => {
            Swal.fire({
              icon: 'error',
              title: 'Não foi possível cadastrar o Cliente',
              showConfirmButton: true,
            });
          }
        );
      }
    }
  }

  closeDialog() {
    this.dialogRef.close();
    this.formCadastro.reset();
  }
}
