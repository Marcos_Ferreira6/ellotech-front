import { JsonPipe } from '@angular/common';

export class Cliente {
  id: number;
  nome: string;
  email: string;
  telefone: string;
  sexo: string;
  cpf: string;
  data_nascimento: string;
}
