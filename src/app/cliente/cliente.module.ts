import { ClienteRoutingModule } from './cliente.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroClienteComponent } from './cadastro-cliente/cadastro-cliente.component';
import { ListaClienteComponent } from './lista-cliente/lista-cliente.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClienteService } from './service/cliente.service';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { DataTablesModule } from 'angular-datatables';
import {
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatTableModule,
  MatPaginatorModule,
  MatDialogModule,
  MatFormFieldModule,
  MatSelectModule,
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule
} from '@angular/material';

export let options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
  declarations: [CadastroClienteComponent, ListaClienteComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClienteRoutingModule,
    HttpClientModule,
    DataTablesModule,
    NgxMaskModule.forRoot(),
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  exports: [CadastroClienteComponent, ListaClienteComponent],
  providers: [ClienteService]
})
export class ClienteModule { }
