import { ClienteDataSource } from './../service/cliente.datasource';
import { CadastroClienteComponent } from './../cadastro-cliente/cadastro-cliente.component';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, Output, EventEmitter } from '@angular/core';
import { ClienteService } from '../service/cliente.service';
import { Cliente } from '../model/cliente.model';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort, PageEvent } from '@angular/material';
import Swal from 'sweetalert2';
import { merge, fromEvent } from 'rxjs';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';


@Component({
  selector: 'app-lista-cliente',
  templateUrl: './lista-cliente.component.html',
  styleUrls: ['./lista-cliente.component.css']
})
export class ListaClienteComponent implements OnInit, AfterViewInit {
  count: number;

  colunas: string[] = ['id', 'nome', 'CPF', 'Editar', 'Deletar'];
  clientes: ClienteDataSource;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('input', { static: true }) input: ElementRef;

  @Output() page: EventEmitter<PageEvent>;

  constructor(
    private clienteService: ClienteService,
    private dialog: MatDialog
  ) { }

  async ngOnInit() {
    this.clientes = new ClienteDataSource(this.clienteService);
    await this.countClientes();
    this.clientes.carregarClientes('', 0, 2);
  }

  ngAfterViewInit() {
    // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;

          this.carregarClientes();
        })
      ).subscribe();


    console.log(this.clientes);

    // merge(this.sort.sortChange, this.paginator.page)
    //   .pipe(
    //     tap(() => this.carregarClientes())
    //   ).subscribe();
  }

  onClickNext(event) {
    this.clientes.carregarClientes(
      this.input.nativeElement.value,
      event.pageIndex,
      event.pageSize
    );
  }

  carregarClientes() {
    this.clientes.carregarClientes(
      this.input.nativeElement.value,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
  }

  async countClientes() {
    this.count = await this.clienteService.count().toPromise();
  }

  async deleteCliente(id: string) {
    Swal.fire({
      title: 'Tem certeza?',
      text: 'Você não será capaz de reverter esta ação',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'SIM',
      cancelButtonText: 'CANCELAR'
    }).then((result) => {
      if (result.value) {
        this.clienteService.delete(id).subscribe(
          async (success) => {
            Swal.fire({
              icon: 'success',
              title: 'O registro foi excluído',
              showConfirmButton: false,
            });
            this.paginator.pageIndex = 0;
          },
          (error) => {
            Swal.fire({
              icon: 'error',
              title: 'Não foi possível exlcuir o registro',
              showConfirmButton: false,
            });
          }
        );
      }
    });
  }

  openDialogCliente(clienteSelecionado?: Cliente): void {
    const dialogRef = this.dialog.open(CadastroClienteComponent, {
      width: '350px', data: { cliente: clienteSelecionado }
    });

    dialogRef.afterClosed().subscribe(result => {
      window.location.reload();
    });
  }

}
