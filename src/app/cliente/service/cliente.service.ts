import { environment } from './../../../environments/environment';
import { Cliente } from './../model/cliente.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GenericService } from 'src/app/shared/generic.service';
import { take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClienteService extends GenericService<Cliente>{

  constructor(public http: HttpClient) {
    super(
      http,
      environment.url_api + '/clientes'
    );
  }

  public existsByCpf(cpf: string) {
    return this.http.get(`${this.url}/exists?cpf=${cpf}`).pipe(take(1));
  }

  public count() {
    return this.http.get<number>(`${this.url}/count`).pipe(take(1));
  }
}
