import { Cliente } from './../model/cliente.model';
import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';
import { Observable, BehaviorSubject } from 'rxjs';
import { ClienteService } from './cliente.service';

export class ClienteDataSource implements DataSource<Cliente> {
  private clienteSubject = new BehaviorSubject<Cliente[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  constructor(private clienteService: ClienteService) { }

  carregarClientes(search: string, page: number, size: number) {
    this.loadingSubject.next(true);

    this.clienteService.search(search, page, size).subscribe(
      response => {
        this.clienteSubject.next(response);
      }
    );
  }

  connect(collectionViewer: CollectionViewer): Observable<Cliente[]> {
    return this.clienteSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.clienteSubject.complete();
    this.loadingSubject.complete();
  }

}
