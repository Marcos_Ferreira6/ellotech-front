import { ListaClienteComponent } from './lista-cliente/lista-cliente.component';
import { CadastroClienteComponent } from './cadastro-cliente/cadastro-cliente.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const routes: Routes = [
  { path: 'lista-clientes', component: ListaClienteComponent },
  { path: 'novo-cliente', component: CadastroClienteComponent },
  { path: 'editar-cliente/:id', component: CadastroClienteComponent }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
