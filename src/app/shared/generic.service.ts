import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { take, map } from 'rxjs/operators';
import { Cliente } from '../cliente/model/cliente.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class GenericService<Objeto> {

  constructor(
    public http: HttpClient,
    @Inject(String) public url
  ) { }

  public findAll(page: number, size: number) {
    return this.http.get<any>(`${this.url}?page=${page}&size=${size}`).pipe(take(1));
  }

  public findById(id: number) {
    return this.http.get<any>(`${this.url}/${id}`).pipe(take(1));
  }

  public search(search: string, page: number, size: number): Observable<Cliente[]> {
    return this.http.get<any>(`${this.url}/search?searchTerm=${search}&page=${page}&size=${size}`).pipe(
      map((res) => {
        return res.content;
      })
    );
  }

  public post(objeto: Objeto) { return this.http.post<any>(`${this.url}`, objeto).pipe(take(1)); }

  public put(objeto: Objeto, id: number) {
    return this.http.put<any>(`${this.url}/${id}`, objeto).pipe(take(1));
  }

  public delete(id: string) { return this.http.delete(`${this.url}/${id}`); }
}
